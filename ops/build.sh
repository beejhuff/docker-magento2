#!/bin/sh

set -e

test -z $1 && echo "[Error] Missing Image tag" && exit 1
test -z $2 && echo "[Error] Missing Magento public access key" && exit 1
test -z $3 && echo "[Error] Missing Magento private access key" && exit 1
test -z $4 && echo "[Warning] Using latest version..."

docker build \
    --tag $1 \
    --build-arg MAGENTO_ACCESS_KEY_PUBLIC=$2 \
    --build-arg MAGENTO_ACCESS_KEY_PRIVATE=$3 \
    --build-arg MAGENTO_CE_VERSION=$4 \
    .
