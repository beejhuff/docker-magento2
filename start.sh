#!/bin/sh

magento setup:upgrade
magento setup:static-content:deploy
magento cache:clean

set -e

php-fpm --allow-to-run-as-root & \
nginx -g 'daemon off;'
